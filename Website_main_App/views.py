from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from .forms import ContactForm



def home_page(request):
    
    context = {}
   
    return render(request, "home_page.html", context)
