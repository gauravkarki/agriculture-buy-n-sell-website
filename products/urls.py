from django.conf.urls import url

from .views import (
        ProductListView,
        ProductDetailSlugView,
        ProductEditView,
        ProductDeleteView
        # , 
        # ProductDownloadView
        )

urlpatterns = [
    url(r'^$', ProductListView.as_view(), name='list'),
    url(r'^(?P<slug>[\w-]+)/$', ProductDetailSlugView.as_view(), name='detail'),
    url(r'^(?P<slug>[\w-]+)/edit-product$', ProductEditView.as_view(), name='edit'),
    url(r'^(?P<slug>[\w-]+)/delete-product$', ProductDeleteView.as_view(), name='delete'),
    # url(r'^(?P<slug>[\w-]+)/(?P<pk>\d+)/$', ProductDownloadView.as_view(), name='download'),
]

