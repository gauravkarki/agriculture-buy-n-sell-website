from django.db import models

# Create your models here.
class Predicted(models.Model):#test model which did not work  can be deleted
    date = models.TextField(blank=True, null=True)  # This field type is a guess.
    quantity = models.TextField(blank=True, null=True)  # This field type is a guess.
    item_no = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'predicted'

class Predict(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    date = models.TextField(blank=True, null=True)  # This field type is a guess.
    quantity = models.TextField(blank=True, null=True)  # This field type is a guess.
    item_no = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'predict'