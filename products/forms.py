from django import forms
from django.db import transaction

from .models import Product

class ProductEditForm(forms.ModelForm):
    class Meta:
    	model = Product
    	fields = ('title', 'description', 'price',  )

    def save(self, commit=True):
    	product = super(ProductEditForm, self).save(commit=False)
    		
    	if commit:
    		product.added_by = self.user.email
    		product.save()
    	return product


class ProductEditForm2(forms.ModelForm):
    class Meta:
        model = Product
        fields = ('title', 'description', 'price', 'price_unit', 'image', 'availability'  )


# 	title			= forms.CharField()
# 	description     = forms.CharField(widget=forms.Textarea)
# 	price           = forms.DecimalField(decimal_places=2, max_digits=20)
    
#     class Meta:
#         model = Product
#         fields = ('email',)

# class CustomerSignUpForm(forms.ModelForm):

#     password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
#     password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

#     class Meta:
#         model = User
#         fields = ('email',)

#     def clean_password2(self):
#         # Check that the two password entries match
#         password1 = self.cleaned_data.get("password1")
#         password2 = self.cleaned_data.get("password2")
#         if password1 and password2 and password1 != password2:
#             raise forms.ValidationError("Passwords don't match")
#         return password2

#     @transaction.atomic
#     def save(self, commit=True):
#         # Save the provided password in hashed format
#         user = super(CustomerSignUpForm, self).save(commit=False)
#         user.set_password(self.cleaned_data["password1"])
#         user.is_active = False # send confirmation email via signals
#         # obj = EmailActivation.objects.create(user=user)
#         # obj.send_activation_email()
#         if commit:
#             user.is_customer = True
#             user.save()
#         return user