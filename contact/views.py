from django.shortcuts import render
from django.views.generic import CreateView
from .models import Contact
from .forms import ContactForm
from django.contrib import messages
from django.shortcuts import render,redirect


class ContactView(CreateView):
    template_name = 'contact/add.html'

    def get(self, request):
        form = ContactForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = ContactForm(request.POST)
        if form.is_valid():
            product = form.save(commit=False)
            product.save()
            form = ContactForm()
            messages.success(request, 'Message Received')
            return redirect('/contact', message='Message Received')

        args = {'form': form}
        return render(request, self.template_name, args)
