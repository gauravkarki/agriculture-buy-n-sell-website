from django.shortcuts import render
from .models import Predict
# Create your views here.
#for SalesAjaxView
from django.http import JsonResponse
from django.views.generic import View
import math
#from datetime import datetime

# def predictshow(request,pk=None):
def predictshow(request):
    querySet=Predict.objects.values('item_no').distinct()
    ls=[]
    for i in querySet:
        ls.append(i['item_no'])
        
    args = {'list':ls,}
    
        
    return render(request,"predict.html",args)

class SalesAjaxView(View):
    template_name='predict.html'
    
    def get(self,request,*args,**kwargs):
        pk=self.kwargs['pk']
        pks=str(pk)
        quantity = Predict.objects.values_list('quantity', flat=True).filter(item_no__exact=pks)
        quantity = list(quantity)
        quantity=[math.floor(float(i)) for i in quantity]
        date = Predict.objects.values_list('date', flat=True).filter(item_no__exact=pks)
        date = list(date)
        date=[i[:10] for i in date]
        #print(date)
      
        data ={}
        data['data'] = quantity
        data['labels'] = date
        return JsonResponse(data)
# url(r'^predict/api/(?P<pk>\d+)/$',SalesAjaxView.as_view(),name='predict-data'),